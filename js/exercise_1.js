/* 
(*) SALARY CALCULATOR

Input
- The variable "oneDaySalary" stores the daily salary that the users input.
- The variable "workDayNumber" stores the number of working days that the users input.

Todo
- The "salary" variable stores the user's salary, which is calculated by multiplying the daily salary by the number of working days.
- salary = oneDaySalary * workDayNumber

Output
- The result of "salary" variable will be shown to the users
*/

function calcSalary() {
  let oneDaySalary = document.querySelector("#one-day-salary").value * 1;
  let workDayNumber = document.querySelector("#work-days").value * 1;
  let salary = new Intl.NumberFormat("vn-VN").format(
    oneDaySalary * workDayNumber
  );
  document.querySelector(
    "#result_salary"
  ).innerHTML = `Your salary this month: <strong>${salary} VND</strong>.`;
}
