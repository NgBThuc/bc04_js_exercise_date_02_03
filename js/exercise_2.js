/* 
(*) AVERAGE CALCULATOR

Input
- 5 integers are entered by the users to determine their average, and they are saved in the variables "num1" through "num5", accordingly.

Todo
- The "average" variable contains the average, which is calculated by dividing the sum of five integers by five.
- average = (num1 + num2 + num3 + num4 + num5) / 5

Output
- The result of the average variable is shown to the users

*/

function calcAverage() {
  let num1 = document.querySelector("#first_number").value * 1;
  let num2 = document.querySelector("#second_number").value * 1;
  let num3 = document.querySelector("#third_number").value * 1;
  let num4 = document.querySelector("#fourth_number").value * 1;
  let num5 = document.querySelector("#fifth_number").value * 1;
  let average = (num1 + num2 + num3 + num4 + num5) / 5;
  document.querySelector(
    "#result_average"
  ).innerHTML = `Average of abover number: <strong>${average.toFixed(
    2
  )}</strong>.`;
}
