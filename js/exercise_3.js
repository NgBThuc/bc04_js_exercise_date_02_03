/* 
(*) MONEY EXCHANGE

Constant
- 1 USD is equal to 23,500 VND and is stored in "usdPrice" variable

Input
- Users input the amount of USD to convert to VND. The USD amount is stored in "usdAmount" variable

To-do
- The USD amount multiplied by the USD price equals the VND amount.
- vndAmount = usdAmount * usdPrice

Output
- The result of variable "vndAmount" is shown to the users
*/

function exchangeMoney() {
  const usdPrice = 23500;
  let usdAmount = document.querySelector("#usd_amount").value * 1;
  let vndAmount = new Intl.NumberFormat("vn-VN").format(usdAmount * usdPrice);
  document.querySelector(
    "#result_exchange"
  ).innerHTML = `Viet Nam Dong amount: <strong>${vndAmount} VND</strong>.`;
}
