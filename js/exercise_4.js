/* 
(*) RECTANGLE AREA AND PERIMETER CALCULATOR

Input
- The variable "rectangleLength" store the length of the rectangle entered by the users
- The variable "rectangleWidth" store the width of the rectangle entered by the users

To-do
- The rectangle's area is calculated by multiplying the length by the width and it is stored in variable "rectangleArea"
- rectangleArea = rectangleLength * rectangleWidth
- The rectangle's perimeter is calculated by multiplying the total of the length and the width by 2 and it is stored in variable "rectanglePerimeter"
- rectanglePerimeter = (rectangleLength + rectangleWidth) * 2

Output
- The result of variables "rectangleArea" and "rectanglePerimeter" is shown to the users
*/

function calcArea() {
  let rectangleLength = document.querySelector("#rectangle_length").value * 1;
  let rectangleWidth = document.querySelector("#rectangle_width").value * 1;
  const rectangleArea = rectangleLength * rectangleWidth;
  document.querySelector(
    "#result_rectangle"
  ).innerHTML = `Area of this rectangle: <strong>${rectangleArea}</strong>.`;
}

function calcPerimeter() {
  let rectangleLength = document.querySelector("#rectangle_length").value * 1;
  let rectangleWidth = document.querySelector("#rectangle_width").value * 1;
  const rectanglePerimeter = (rectangleLength + rectangleWidth) * 2;
  document.querySelector(
    "#result_rectangle"
  ).innerHTML = `Perimeter of this rectangle: <strong>${rectanglePerimeter}</strong>.`;
}
