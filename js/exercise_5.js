/* 
(*) SUM OF TWO DIGITS OF TWO-DIGIT NUMBER

Input
- Users enters two-digit number

To-do
- By dividing a two-digit number by 10 and rounding the result, the first digit of a two-digit number can be calculated.
- firstNum = Math.floor(targetNum / 10)
- The second digit of a two-digit number is determined by dividing the remainder by 10.
- secondNum = targetNum % 10
- And sum of two digits of two-digit number is determined by the total of "firstNum" and "secondNum"
- total = firstNum + secondNum

Output
- The result of varible "total" is shown to the users
*/

function sumDigit() {
  let targetNum = document.querySelector("#target_number").value * 1;
  let firstNum = Math.floor(targetNum / 10);
  let secondNum = targetNum % 10;
  let total = firstNum + secondNum;
  document.querySelector(
    "#result_total"
  ).innerHTML = `Total of 2 digits: <strong>${total}</strong>.`;
}
